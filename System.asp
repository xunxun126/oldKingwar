<!--#include file="inc/Biz_Inc.asp" -->
<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="14" valign="top" background="Images/2007/OWARSbb.jpg"><img name="OWARSbb" src="Images/2007/OWARSbb.jpg" width="14" height="211" border="0" id="OWARSbb" alt="" /></td>
    <td width="732" valign="top" bgcolor="#F4EBDA"><table width="732" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" valign="top"><table width="696" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="696"><table border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td>&nbsp;</td>
                <td height="30" class="menu_71"><p align="center"><STRONG> 《三国义志》游戏规则及管理员工作守则</STRONG></p></td>
              </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td>&nbsp;</td>
                  <td valign="top"><p class="menu_71">一、<span style="font-weight: bold">基本原则</span> </p>
                      <p>&nbsp;&nbsp;&nbsp; 以下所述的基本原则仅适用于《三国义志》(下简三网)，三网将在此原则基础上，为《三国义志》用户提供客户服务。 </p>
                    <p>《三国义志》—拥有对此原则的制定、修改及删除的权利。当此原则的具体内容发生变更，会将更改内容通过网站公告的形式通知用户，我们不对未正确阅读本原则及其相关修改内容带来的损失负任何责任。 </p>
                    <p>&nbsp;&nbsp;&nbsp; 用户必须遵守三网所制定的各项制度与规范，凡在游戏中进行与制度、规范相违背的行为，将受到警告 ; 在接到警告后仍继续做出相同行为的用户，我们有权对其做出各种程度的停权处罚（包括永久停权）。对于在游戏中造成严重及恶劣影响的行为，三网可以直接终止该用户的帐号使用权。 </p>
                    <p>管理员作为三网的合法代表，负责维护游戏世界的平衡与公正。管理员拥有对用户非法行为纠正及警告的权力，并且可以根据游戏管理条例对违规用户做出相应处罚。 </p>
                    <p>在本原则中未提及的事项，根据管理员和用户的一般性社会概念判断和处理。 </p>
                    <p>在本原则中未明示的事项，将根据使用条款和相关法规做出处理。 </p>
                    <p class="menu_71">二、<span style="font-weight: bold">游戏规则定义</span></p>
                    <p class="style1">以下行为均视为违规行为： </p>
                    <p>1.在游戏中持续性地使用带有人身攻击性的语句、恶意刷屏行为 ; <br>
                      2.在游戏中建立带有反社会、反道德性质的用户名及门派帮会名称 ; <br>
                      3.在游戏及官方论坛中以任何形式散布或传播诽谤、侮辱、具威胁性、攻击性、不雅、猥亵、不实、违反公共秩序的非法文字或图片 ; <br>
                      4.直接或间接利用程序BUG牟利或侵害其他玩家权益 ;<br>
                      5.使用或传播任何第三方非法软件的行为 ; <br>
                      6.分析、修改或反编译游戏执行程序或数据的行为，破坏游戏完整性的行为，监听、游戏客户端与服务端通讯过程中的数据包，从而影响正常的B/S通讯的行为 ;<br>
                      7.冒用开发商、发行商和三网职员名义的行为 ; <br>
                      8.利用与其他玩家相似用户名在游戏中进行欺诈的行为 ; <br>
                      9.盗取他人游戏帐号、物品的行为 ; <br>
                      10.虚假举报以及滥用问题反应原则的行为 ;<br>
                      11.帐号或虚拟物品的现金交易 ; <br>
                      12.三网与其他用户公认的，需要加以制裁的行为。 </p>
                    <p class="menu_71">三、<span style="font-weight: bold">管理员受理问题范围</span></p>
                    <p class="style1">以下问题，不在管理员受理范围内： </p>
                    <p>1.用户在注册时填写不实资料或将其数据如：游戏帐号、密码、注册信息等资料泄漏或以有偿、无偿方式转让与他人时，将由用户自行承担所有因此可能带来的损失。管理员目前暂不受理任何被盗申诉以及非相似用户名被骗申诉，请玩家妥善保管自己的游戏帐号，并定期更换密码。 <br>
                      2.用户可在官方主页上进行密码找回，管理员不受理密码人工找回业务。 <br>
                      3.管理员不受理用户注册证件号修改业务，作为用户的基本身份标识，用户本人也无法修改注册证件号码，请用户在进行注册时务必妥善填写。</p>
                    <p class="style1">管理员受理问题分类及时限： </p>
                    <table height="253" border="1" align="center" cellpadding="3" cellspacing="0" bordercolor="#999999">
                        <tr>
                          <td><p align="center" class="menu_71">问题类型 </p></td>
                          <td><p align="center" class="menu_71">问题受理时限 </p></td>
                          <td><p align="center" class="menu_71">问题回复时限 </p></td>
                          <td><p align="center" class="menu_71">用户需提交资料 </p></td>
                        </tr>
                        <tr>
                          <td><p>密码保护修改或寻回 </p></td>
                          <td><p align="center">无时限 </p></td>
                          <td><p align="center">一个工作日 </p></td>
                          <td><p>当前密码保护或注册资料中身份证复印件 </p></td>
                        </tr>
                        <tr>
                          <td><p>违规事件举报 </p></td>
                          <td><p align="center">当天 </p></td>
                          <td><p align="center">三个工作日 </p></td>
                          <td><p>举报人ID，所在服务器，联系电话，举报内容以及事件发生时间 </p></td>
                        </tr>
                        <tr>
                          <td><p>BUG及系统问题报告 </p></td>
                          <td><p align="center">无时限 </p></td>
                          <td><p align="center">三个工作日 </p></td>
                          <td><p>BUG类型描述，相关游戏用户名，所在服务器，问题出现时间，以及出现问题的条件 </p></td>
                        </tr>
                        <tr>
                          <td><p>因相似用户名被骗 </p></td>
                          <td><p align="center">一天以内 </p></td>
                          <td><p align="center">三个工作日 </p></td>
                          <td><p>举报人ID，所在服务器，联系电话，本应交易的用户名，所举报用户名，及交易发生时间，交易物品属性及数量 </p></td>
                        </tr>
                      </table>
                    <p class="style1"><span style="font-weight: bold">注释</span>： </p>
                    <p>1.问题类型：管理员可受理问题的类型，不在此范围内，管理员将不受理。<br>
                      2.问题受理时限：玩家需要在问题发生后相应时限内向管理员申诉，超出申诉时限的问题管理员不受理。<br>
                      3.问题处理时限：管理员处理或向玩家反馈事件结果、进程的时限。<br>
                      4.用户需要提交资料：玩家申诉问题时，必须向管理员提交除用户基本信息而外的相应资料，如不能提供，管理员将不受理。</p>
                    <p class="menu_71">四、<span style="font-weight: bold">游戏管理条例</span> </p>
                    <p>游戏管理条例首先以社会道德以及相关的法律、法规为依据，在此基础上充分考虑游戏世界的特殊情况与环境，由管理员执行，保证游戏世界的平衡与稳定。如出现与管理条例相违背的行为，将根据条例中的相关规定给予处罚。对于个别情节特别严重、影响极为恶劣的行为， 管理员可以不考虑违规次数，直接将相关帐号进行封停。 </p>
                    <table height="255" border="1" align="center" cellpadding="3" cellspacing="0" bordercolor="#999999">
                        <tr class="style2">
                          <td><p align="center" class="menu_71">事件类型 </p></td>
                          <td><p align="center" class="menu_71">第一次 </p></td>
                          <td><p align="center" class="menu_71">第二次 </p></td>
                          <td><p align="center" class="menu_71">第三次 </p></td>
                        </tr>
                        <tr>
                          <td><p align="left">使用外挂程序或修改游戏 </p></td>
                          <td><p align="center">永久停权 </p></td>
                          <td><div align="center"> </div></td>
                          <td><div align="center"> </div></td>
                        </tr>
                        <tr>
                          <td><p align="left">冒充管理员或三网工作人员 </p></td>
                          <td><p align="center">永久停权 </p></td>
                          <td><div align="center"> </div></td>
                          <td><div align="center"> </div></td>
                        </tr>
                        <tr>
                          <td><p align="left">利用与其他玩家相似用户名在游戏中进行欺诈 </p></td>
                          <td><p align="center">停权一个月 </p></td>
                          <td><p align="center">永久停权 </p></td>
                          <td><div align="center"> </div></td>
                        </tr>
                        <tr>
                          <td height="33"><p align="left">利用游戏BUG牟利或影响其他玩家正常游戏 </p></td>
                          <td><p align="center">停权一个月 </p></td>
                          <td><p align="center">永久停权 </p></td>
                          <td><div align="center"> </div></td>
                        </tr>
                        <tr>
                          <td><p align="left">在游戏中对其他玩家进行谩骂或恶意刷屏 </p></td>
                          <td><p align="center">警告 </p></td>
                          <td><p align="center">停权一个月 </p></td>
                          <td><p align="center">永久停权 </p></td>
                        </tr>
                        <tr>
                          <td><p align="left">用户名或门派中包含谩骂、色情等非法内容 </p></td>
                          <td><p align="center">删除人物或门派</p></td>
                          <td><div align="center"> </div></td>
                          <td><div align="center"> </div></td>
                        </tr>
                        <tr>
                          <td><p align="left">虚假举报或滥用问题反映原则 </p></td>
                          <td><p align="center">停权一个月 </p></td>
                          <td><p align="center">永久停权 </p></td>
                          <td><div align="center"> </div></td>
                        </tr>
                      </table>
                    <p>以上各项条款，《<span style="font-weight: bold">三国义志</span>》拥有最终的解释权。如有与法律条款有相抵触的内容，以法律条款为准。 </p>
                    <p>&nbsp;</p></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
    <td width="14" valign="top" background="Images/2007/OWARSbf.jpg"><img name="OWARSbf" src="Images/2007/OWARSbf.jpg" width="14" height="388" border="0" id="OWARSbf" alt="" /></td>
  </tr>
</table>
<!--#include file="inc/Biz_IncFooter.asp" -->


