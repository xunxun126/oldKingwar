<!--#include file="inc/Biz_Inc.asp" -->
<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="14" valign="top" background="Images/2007/OWARSbb.jpg"><img name="OWARSbb" src="Images/2007/OWARSbb.jpg" width="14" height="211" border="0" id="OWARSbb" alt="" /></td>
    <td width="732" valign="top" bgcolor="#F4EBDA"><table width="732" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" valign="top"><DIV align=center>
          <table width="400" height="42" border="0" cellpadding="10" cellspacing="1" bgcolor="#CABF8E">
            <tr>
              <td align="center" bgcolor="#EBE7D3"><strong>三国义志 新手上路2 </strong></td>
            </tr>
          </table>
          <table width="50%"  border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <TABLE cellSpacing=0 cellPadding=0 width=650 align=center border=0>
            <TBODY>
              <TR>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step1.asp"><IMG 
            src="images/guide_step_b1.gif" width="55" height="29" border=0><BR>
                      <FONT 
            color=#a6a6a6><B>登陆方法</B></FONT></A></TD>
                <TD align=middle width=15><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step2.asp"><IMG 
            src="images/guide_step2.gif" width="55" border=0><BR>
                      <FONT 
            color=#5d5d5d><B>选择君主</B></FONT></A></TD>
                <TD align=middle width=14><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step3.asp"><IMG 
            height=29 src="images/guide_step_b3.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>国家初期建设</B></FONT></A></TD>
                <TD align=middle width=14><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step4.asp"><IMG 
            height=29 src="images/guide_step_b4.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>录用将帅</B></FONT></A></TD>
                <TD align=middle width=14><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step5.asp"><IMG 
            height=29 src="images/guide_step_b5.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>战争准备</B></FONT></A></TD>
                <TD align=middle width=14><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step6.asp"><IMG 
            height=29 src="images/guide_step_b6.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>战争</B></FONT></A></TD>
                <TD align=middle width=15><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step7.asp"><IMG 
            height=29 src="images/guide_step_b7.gif" width=55 
            border=0><BR>
                      <FONT 
      color=#a6a6a6><B>其他</B></FONT></A></TD>
              </TR>
            </TBODY>
          </TABLE>
          <table width="400" height="18" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <TABLE cellSpacing=0 cellPadding=0 width="90%" border=0>
            <TBODY>
              <TR>
                <TD colSpan=2><FONT color=#fb3f00><B>2. 选择君主 </B></FONT></TD>
              </TR>
              <TR>
                <TD colSpan=2><p><FONT color=#fb3f00>&nbsp;&nbsp;</FONT></p>
                  <p><FONT color=#fb3f00>&nbsp;<img src="Images/Help/lgoingame3.gif" width="471" height="291"></FONT></p>
                  <p><strong><font color="#006699">各系列的简单介绍:</font></strong></p>
                  <p><strong>刘备</strong>：刘是折中主义者。经济情况中等偏下，刚好达到各个系列的平均。刘有感怀百姓，所以宝物基本不愁，至少比其他系列好的多。刘有最强攻击阵，但在下面却往往是大家欺负的对象。他什么样的计策都基本上有，但往往比别人要差那么一点，（当然，比没有的好多了）。刘找将由于和曹操系列有冲突，所以对于曹操系列的将领存在一定的难度，但刘系列本身又是好将最多的。刘是典型的折中主义者，但同时也促进了刘是万金油，到那里都可以。</p>
                  <p><BR>
                    <strong>曹操</strong>：一等的经济状况，最好的粮食政策，最强防守阵，几大非常好用的战前政策，还算可以的攻击型计策等等都促进了曹操将是以后高位的强有力的争夺者，唯一的缺陷是曹操系列和刘系列在将领雇佣上有冲突，要得到刘系列的众多好将有一点的难度，但曹操系列有持续的招将政策九品，可以很大程度上缓和这一缺陷。</p>
                  <p><BR>
                    <strong>张鲁</strong>：最销帐的战前计策，形形色色占据着多项之最，论靠阵杀敌，谁能与敌，唯一的遗憾是缺乏一个很好的攻击型计策。而且其中经济情况也较差，不容易维持。预计将成为国土大户，和董卓共同充当土匪的角色。</p>
                  <p><BR>
                    <strong>袁绍</strong>：一落星遮百臭加上还算不错的经济状况，使不少人蠢蠢欲动了</p>
                  <p><BR>
                    <strong>董卓</strong>：董卓是个进攻狂，他的将天生带攻城，既加20%的攻击力，再加上来上几个吕布登场，进攻那还不是手到擒来的啊，不过其最为糟糕的经济状况，注定了他只是土匪命。</p>
                  <p><BR>
                    <strong>孙权</strong>：用上几个苦肉计，最狂妄的攻击型计策，再加上自己的守城特性，也就是防守加20%的攻击力，防守应该是没有太大的问题的，还算可以的经济情况，做个地主或者国土仓库挺不错的。<BR>
                  </p></TD>
              </TR>
              <TR>
                <TD><FONT color=#006699></FONT></TD>
                <TD><BR>
                    <FONT 
            color=#006699>建立国家前需要完成两个步骤</FONT><BR>
                  1填写君主的名字.也就是为自己起一个非常个性化的名字。（同名的君主无法使用）.<BR>
                  2选择君主系列-----君主系列有:曹操.刘备.孙权.张鲁.袁绍.董卓。<BR>
                  <BR>
                  选择不同的君主相应的政策与阵法等也会不同。希望您能选择一个适合自己的君主。 </TD>
              </TR>
            </TBODY>
          </TABLE>
        </DIV></td>
      </tr>
    </table></td>
    <td width="14" valign="top" background="Images/2007/OWARSbf.jpg"><img name="OWARSbf" src="Images/2007/OWARSbf.jpg" width="14" height="388" border="0" id="OWARSbf" alt="" /></td>
  </tr>
</table>
<!--#include file="inc/Biz_IncFooter.asp" -->


