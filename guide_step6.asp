<!--#include file="inc/Biz_Inc.asp" -->
<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="14" valign="top" background="Images/2007/OWARSbb.jpg"><img name="OWARSbb" src="Images/2007/OWARSbb.jpg" width="14" height="211" border="0" id="OWARSbb" alt="" /></td>
    <td width="732" valign="top" bgcolor="#F4EBDA"><table width="732" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" valign="top"><DIV align=center>
          <table width="400" height="42" border="0" cellpadding="10" cellspacing="1" bgcolor="#CABF8E">
            <tr>
              <td align="center" bgcolor="#EBE7D3"><strong>三国义志 新手上路6 </strong></td>
            </tr>
          </table>
          <table width="50%"  border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <TABLE cellSpacing=0 cellPadding=0 width=650 align=center border=0>
            <TBODY>
              <TR>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step1.asp"><IMG 
            src="images/guide_step_b1.gif" border=0><BR>
                      <FONT 
            color=#a6a6a6><B>登陆方法</B></FONT></A></TD>
                <TD align=middle width=15><IMG src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step2.asp"><IMG 
            src="images/guide_step_b2.gif" border=0><BR>
                      <FONT 
            color=#a6a6a6><B>选择君主</B></FONT></A></TD>
                <TD align=middle width=14><IMG src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step3.asp"><IMG 
            height=29 src="images/guide_step_b3.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>国家初期建设</B></FONT></A></TD>
                <TD align=middle width=14><IMG src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step4.asp"><IMG 
            height=29 src="images/guide_step_b4.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>录用将帅</B></FONT></A></TD>
                <TD align=middle width=14><IMG src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step5.asp"><IMG 
            height=29 src="images/guide_step_b5.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>战争准备</B></FONT></A></TD>
                <TD align=middle width=14><IMG src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step6.asp"><IMG 
            height=29 src="images/guide_step6.gif" width=55 border=0><BR>
                      <FONT 
            color=#5d5d5d><B>战争</B></FONT></A></TD>
                <TD align=middle width=15><IMG src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step7.asp"><IMG 
            height=29 src="images/guide_step_b7.gif" width=55 
            border=0><BR>
                      <FONT 
      color=#a6a6a6><B>其他</B></FONT></A></TD>
              </TR>
            </TBODY>
          </TABLE>
          <table width="400" height="18" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <TABLE cellSpacing=0 cellPadding=0 width="90%" border=0>
            <TBODY>
              <TR>
                <TD colSpan=2 height=25><FONT color=#fb3f00><FONT 
            color=#fb3f00><B>6. 战争</B></FONT> <FONT 
            color=#006699>&nbsp;&nbsp;&nbsp;&nbsp;选择攻击国 &gt; 出征部队 &gt; 战争</FONT> </FONT></TD>
              </TR>
              <TR>
                <TD colSpan=2 height=25><FONT color=#fb3f00>&nbsp;&nbsp; (1)出征 </FONT></TD>
              </TR>
              <TR>
                <TD width="5%"></TD>
                <TD><p>选定攻击目标、攻击方法（二种）。<BR>
                  <BR>
                  <FONT color=#0069b3><B>都市攻略战 : </B></FONT>攻打敌人的首都取得胜利的机会少,但可以得到一些财物和土地。<BR>
                  <FONT 
            color=#0069b3><B>边防攻略战 : </B></FONT>攻打敌人的边防.胜利的机会多,无法得到国土。
                  <BR>
                  <BR>
                  <FONT 
            color=#000000><B>阵法 : </B></FONT>方阵、 圆阵、 雁行阵 、 追行阵、鱼鳞阵 、鹤翼阵、长蛇阵、八门金锁阵、八阵图 。<BR>
                </p>
                  <p><img src="Images/Help/select.gif" width="441" height="363"> </p></TD>
              </TR>
              <TR>
                <TD colSpan=2 height=25><FONT color=#fb3f00>&nbsp;&nbsp;(2) 防御的要点</FONT></TD>
              </TR>
              <TR>
                <TD width="5%"></TD>
                <TD>为了避开敌人的计略和防止遭受打击，应注意政策的实施，如：备有出色的军师防止敌人的计略；勇猛的武将抵挡敌人的侵犯.。<BR>
                    <BR></TD>
              </TR>
              <TR>
                <TD></TD>
                <TD><p><img src="Images/Help/att1.gif" width="602" height="495"></p>
                  <p>&nbsp;</p>
                  <p><img src="Images/Help/att2.gif" width="293" height="187"></p>
                  <p><img src="Images/Help/att3.gif" width="438" height="371"></p>
                  <p><img src="Images/Help/att4.gif" width="437" height="366"></p>
                  <p><img src="Images/Help/att6.gif" width="437" height="366"></p>
                  <p><img src="Images/Help/att5.gif" width="437" height="366"></p>
                  <p><img src="Images/Help/att7.gif" width="437" height="366"></p>
                  <p class="style5">注意:</p>
                  <p>战争结速后,请及时补充武将气力,查看士气和兵力分配情况.不然下次出战可能因为气力不足,士气低下等原因无法取得战争胜利..</p></TD>
              </TR>
            </TBODY>
          </TABLE>
        </DIV></td>
      </tr>
    </table></td>
    <td width="14" valign="top" background="Images/2007/OWARSbf.jpg"><img name="OWARSbf" src="Images/2007/OWARSbf.jpg" width="14" height="388" border="0" id="OWARSbf" alt="" /></td>
  </tr>
</table>
<!--#include file="inc/Biz_IncFooter.asp" -->


