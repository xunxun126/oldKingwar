<!--#include file="inc/Biz_Inc.asp" -->
<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="14" valign="top" background="Images/2007/OWARSbb.jpg"><img name="OWARSbb" src="Images/2007/OWARSbb.jpg" width="14" height="211" border="0" id="OWARSbb" alt="" /></td>
    <td width="732" valign="top" bgcolor="#F4EBDA"><table width="732" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" valign="top"><DIV align=center>
          <table width="400" height="42" border="0" cellpadding="10" cellspacing="1" bgcolor="#CABF8E">
            <tr>
              <td align="center" bgcolor="#EBE7D3"><strong>三国义志 新手上路4 </strong></td>
            </tr>
          </table>
          <table width="50%"  border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <TABLE cellSpacing=0 cellPadding=0 width=650 align=center border=0>
            <TBODY>
              <TR>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step1.asp"><IMG 
            src="images/guide_step_b1.gif" border=0><BR>
                      <FONT 
            color=#a6a6a6><B>登陆方法</B></FONT></A></TD>
                <TD align=middle width=15><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step2.asp"><IMG 
            src="images/guide_step_b2.gif" border=0><BR>
                      <FONT 
            color=#a6a6a6><B>选择君主</B></FONT></A></TD>
                <TD align=middle width=14><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step3.asp"><IMG 
            height=29 src="images/guide_step_b3.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>国家初期建设</B></FONT></A></TD>
                <TD align=middle width=14><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step4.asp"><IMG 
            height=29 src="images/guide_step4.gif" width=55 
            border=0><BR>
                      <FONT color=#5d5d5d><B>录用将帅</B></FONT></A></TD>
                <TD align=middle width=14><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step5.asp"><IMG 
            height=29 src="images/guide_step_b5.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>战争准备</B></FONT></A></TD>
                <TD align=middle width=14><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step6.asp"><IMG 
            height=29 src="images/guide_step_b6.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>战争</B></FONT></A></TD>
                <TD align=middle width=15><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step7.asp"><IMG 
            height=29 src="images/guide_step_b7.gif" width=55 
            border=0><BR>
                      <FONT 
      color=#a6a6a6><B>其他</B></FONT></A></TD>
              </TR>
            </TBODY>
          </TABLE>
          <table width="400" height="18" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <TABLE cellSpacing=0 cellPadding=0 width="90%" border=0>
            <TBODY>
              <TR>
                <TD colSpan=2 height=25><FONT color=#fb3f00><B>4. 将帅录用 </B></FONT></TD>
              </TR>
              <TR>
                <TD width="3%"></TD>
                <TD><FONT color=#000000>可以通过《实施政策》中《推荐》《腾达》《三顾茅庐》等方法获得。最多可以雇佣将帅30名 <BR>
                      <BR>
                      <FONT color=#006699><B>人物 :</B><BR>
                      <BR>
                      <B>曹操 :</B>有九品官人法和推荐<BR>
                      <BR>
                      <B>刘备 :</B>有推荐和三顾茅庐<BR>
                      <BR>
                      <B>孙权 :</B>有推荐和腾达<BR>
                      <BR>
                      <B>张鲁 :</B>只有推荐<BR>
                      <BR>
                      <B>袁绍 :</B>只有推荐<BR>
                      <BR>
                      <B>董卓 :</B>推荐和貂蝉的微笑可以录用将帅<BR>
                      <BR>
                      <B>武力 :</B>在〖单挑〗中将帅一对一的比武，可以看出将帅的能力和指挥能力<BR>
                      <BR>
                      <B>智力 :</B>使用计略的一种能力，如：在〖业务分配〗中将智力高的将帅分配为军师的话,不光在战场上的威力很大,而且会起到阻止对方国计略实施的效果.<BR>
                      <BR>
                      <B>魅力 :</B>君主的魅力是指使用〖救济〗时能否对百姓有好的效果，各将帅是否可以买到便宜的粮食.<BR>
                      <BR>
                      <B>忠诚 :</B>最高值100,如果低于20的话,将帅就会有随时离开君主的可能.<BR>
                      <BR>
                      <B>气力 :</B>将帅出征时所具有的一种能力值。20以上.<BR>
                      <BR>
                      <B>能力值 :</B>体现特殊的将帅能力.<BR>
                      <BR>
                    </FONT></FONT></TD>
              </TR>
            </TBODY>
          </TABLE>
        </DIV></td>
      </tr>
    </table></td>
    <td width="14" valign="top" background="Images/2007/OWARSbf.jpg"><img name="OWARSbf" src="Images/2007/OWARSbf.jpg" width="14" height="388" border="0" id="OWARSbf" alt="" /></td>
  </tr>
</table>
<!--#include file="inc/Biz_IncFooter.asp" -->

