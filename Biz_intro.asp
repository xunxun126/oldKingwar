<!--#include file="inc/Biz_Inc.asp" -->
<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="14" valign="top" background="Images/2007/OWARSbb.jpg"><img name="OWARSbb" src="Images/2007/OWARSbb.jpg" width="14" height="211" border="0" id="OWARSbb" alt="" /></td>
    <td width="732" valign="top" bgcolor="#F4EBDA"><table width="732" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" valign="top"><DIV align=center>
          <table width="400" height="42" border="0" cellpadding="10" cellspacing="1" bgcolor="#CABF8E">
            <tr>
              <td align="center" bgcolor="#EBE7D3"><strong>三国义志 介绍 </strong></td>
            </tr>
          </table>
          <table width="50%"  border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <table width="661" height="42" border="0" cellpadding="10" cellspacing="1" bgcolor="#CABF8E">
            <tr>
              <td bgcolor="#EBE7D3"><div align="left"><FONT color=black>&nbsp;&nbsp;&nbsp; “三国义志”是一款极具内涵的网络策略游戏。游戏的主旨是争霸（PK），在游戏中玩家可以自由取名，选择加入自己喜欢的派系和城池，建立并率领军队攻打敌人、抢夺城池。但现在你面前的敌手将不再是程序中刻意安排的NPC，而是网上的其他玩家，可谓变幻无穷-------“四方盗贼如蚁聚，六合奸雄皆鹰扬”。系统中时时变动的排名榜会带给你更加刺激的成就感和永无止境的争霸欲望。想想与异域玩家斗智斗勇的场面…………<BR>
                        <BR>
                        <FONT 
color=#000000><strong>古朴优美的游戏界面</strong></FONT><BR>
                        <BR>
                &nbsp;&nbsp;&nbsp;&nbsp; 有别于一般的游戏，“龙腾虎跃”无须安装，只要打开IE便可直接进入。游戏的界面继承了历代三国游戏真实而古朴的风格，无论是人物或是画面都让人感觉回到了那动荡的乱世年代公元200年。<BR>
                <BR>
                <strong><FONT 
color=#000000>完善简洁的操作系统</FONT></strong><BR>
                <BR>
                &nbsp;&nbsp;&nbsp;&nbsp; 在操作系统方面，除保留了传统游戏的精华，也加入了新的要素。玩家们可以在游戏中自由的控制城池和军队，再也不用为以天为单位的行动而烦恼了，游戏中系统会赠送玩家300点的能力值，而且每过3分钟，系统会自动恢复一点的能力值，只要有足够的时间，想不强大都很难啊！游戏的时间设定和现实中是完全一样的，而且在线玩家也可以攻打不在线玩家的城池，所以为了照顾上网时间较少的玩家，系统还配有托管的功能，当玩家下线时，系统会接管城防工作，所以玩家不在线时也不用太担心，或许还能打赢升名次呢。<BR>
                <BR>
                <FONT 
color=#000000 face="Verdana, Arial, Helvetica, sans-serif"><strong>PK系统</strong></FONT><BR>
                <BR>
                &nbsp;&nbsp;&nbsp;&nbsp; 和一般的网络游戏一样，当角色达到一定的标准时，玩家就可以PK了。在这款游戏中，除了要求武将和兵力的搭配外，兵法和计谋的运用也被提升到了一个新的高度，PK中，兵力的多少已不是胜负的关键，兵法和计谋才是制胜的法宝。此外，游戏中还有大量的宝物供玩家搜索，除了那些名将的独有兵器外，连现代武器也都参与其中。如果玩家运气好的话就可以一试古今大比拼了………… 太可怕了！<BR>
                <BR>
                <strong>不重剧情、不设NPC，只留下无限空间，游戏已经开始，快快登陆</strong></FONT></div></td>
            </tr>
          </table>
        </DIV></td>
      </tr>
    </table></td>
    <td width="14" valign="top" background="Images/2007/OWARSbf.jpg"><img name="OWARSbf" src="Images/2007/OWARSbf.jpg" width="14" height="388" border="0" id="OWARSbf" alt="" /></td>
  </tr>
</table>
<!--#include file="inc/Biz_IncFooter.asp" -->

