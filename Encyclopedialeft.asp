<HTML><HEAD><TITLE>三国义志</TITLE>
<META http-equiv=Content-Type content="text/html; charset=gb2312">

<STYLE type=text/css>A:link {
	FONT-SIZE: 9pt; COLOR: #ffcc99; LINE-HEIGHT: 17px; TEXT-DECORATION: none
}
A:visited {
	FONT-SIZE: 9pt; COLOR: #ffcc99; TEXT-DECORATION: none
}
A:hover {
	FONT-SIZE: 9pt; COLOR: #ffffcc; TEXT-DECORATION: underline
}
.stext {
	FONT-SIZE: 9pt; COLOR: #777777; LINE-HEIGHT: 14pt
}
.stext1 {
	FONT-SIZE: 8pt; COLOR: #3c3c3c; LINE-HEIGHT: 10pt
}
.stext2 {
	FONT-SIZE: 9pt; COLOR: #f7f3b1; LINE-HEIGHT: 10pt
}
TD {
	FONT-SIZE: 9pt; COLOR: #f7f3b1; LINE-HEIGHT: 11pt
}
.green {
	FONT-SIZE: 9pt; COLOR: #f7f3b1; LINE-HEIGHT: 11pt
}
.bm01 {
	FONT-SIZE: 9pt; COLOR: #fff1c2; LINE-HEIGHT: 11pt
}
.bm02 {
	FONT-SIZE: 9pt; COLOR: #ffddac; LINE-HEIGHT: 11pt
}
</STYLE>

<STYLE type=text/css>BODY {
	FONT-SIZE: 9pt; COLOR: #000000; FONT-FAMILY: ??
}
TABLE {
	FONT-SIZE: 9pt; COLOR: #000000; FONT-FAMILY: ??
}
A:link {
	COLOR: #e4ca85; TEXT-DECORATION: none
}
A:visited {
	COLOR: #e4ca85; TEXT-DECORATION: none
}
A:active {
	COLOR: #d89c12; TEXT-DECORATION: none
}
A:hover {
	COLOR: #e4ca85; TEXT-DECORATION: none
}
</STYLE>

<META content="MSHTML 6.00.2600.0" name=GENERATOR></HEAD>
<BODY 
style="BORDER-RIGHT: black 0px dotted; BORDER-TOP: black 0px dotted; BORDER-BOTTOM: black 0px dotted" 
text=#e4ca85 bgColor=#270705>
<SCRIPT language=javascript>
<!--
var old_menu = '';
var old_cell = '';
function menuclick( submenu ,cellbar)
{
  if( old_menu != submenu ) {
    if( old_menu !='' ) {
      old_menu.style.display = 'none';
    }
    submenu.style.display = 'block';
    old_menu = submenu;
    old_cell = cellbar;

  } else {
    submenu.style.display = 'none';
    old_menu = '';
    old_cell = '';
  }
}
-->
</SCRIPT>

<TABLE style="LEFT: 0px; POSITION: absolute; TOP: 0px" cellSpacing=0 
cellPadding=0 width=110 border=0>
  <TBODY>
  <TR>
    <TD><IMG src="images/ency1.gif"></TD></TR>
  <TR>
    <TD><SPAN language=javascript id=bar1 style="CURSOR: hand" 
      onclick="menuclick(submenu1, bar1 );"><IMG 
      src="images/encyb1.gif" border=0 name=nc></SPAN></TD></TR>
  <TR>
    <TD align=left width=100><SPAN id=submenu1 
      style="DISPLAY: none; MARGIN-LEFT: 5px"><BR><A 
      href="Encyclopedia_GenList.asp?hrType=1&amp;strSort=0" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;刘备系列</A><BR><A 
      href="Encyclopedia_GenList.asp?hrType=2&amp;strSort=0" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;曹操系列</A><BR><A 
      href="Encyclopedia_GenList.asp?hrType=3&amp;strSort=0" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;孙权系列</A><BR><A 
      href="Encyclopedia_GenList.asp?hrType=4&amp;strSort=0" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;董卓系列</A><BR><A 
      href="Encyclopedia_GenList.asp?hrType=5&amp;strSort=0" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;袁绍系列</A><BR><A 
      href="Encyclopedia_GenList.asp?hrType=6&amp;strSort=0" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;张鲁系列</A><BR></SPAN></TD></TR>
  <TR>
    <TD><SPAN language=javascript id=bar2 style="CURSOR: hand" 
      onclick="menuclick(submenu2, bar2 );"><IMG 
      src="images/encyb2.gif" border=0 name=nc1></SPAN></TD></TR>
  <TR>
    <TD align=left width=100><SPAN id=submenu2 
      style="DISPLAY: none; MARGIN-LEFT: 5px"><BR><A 
      href="Encyclopedia_UnitList.asp?UnitType=1" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;步兵系列</A><BR><A 
      href="Encyclopedia_UnitList.asp?UnitType=2" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;弓兵系列</A><BR><A 
      href="Encyclopedia_UnitList.asp?UnitType=3" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;骑兵系列</A><BR><A 
      href="Encyclopedia_UnitList.asp?UnitType=4" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;机械兵系列</A><BR><A 
      href="Encyclopedia_UnitList.asp?UnitType=5" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;守城系列</A><BR></SPAN></TD></TR>
  <TR>
    <TD><SPAN language=javascript id=bar3 style="CURSOR: hand" 
      onclick="menuclick(submenu3, bar3);"><IMG 
      src="images/encyb3.gif" border=0 name=nc2></SPAN> </TD></TR>
  <TR>
    <TD align=left width=100><SPAN id=submenu3 
      style="DISPLAY: none; MARGIN-LEFT: 5px"><BR><A 
      href="Encyclopedia_ItemList.asp?ItemType=1" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;武器系列</A><BR><A 
      href="Encyclopedia_ItemList.asp?ItemType=2" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;书系列</A><BR><A 
      href="Encyclopedia_ItemList.asp?ItemType=3" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;宝物系列</A><BR><A 
      href="Encyclopedia_ItemList.asp?ItemType=4" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;马系列</A><BR></SPAN></TD></TR>
  <TR>
    <TD><SPAN language=javascript id=bar4 style="CURSOR: hand" 
      onclick="menuclick(submenu4, bar4);"><IMG 
      src="images/encyb4.gif" border=0 name=nc3></SPAN> </TD></TR>
  <TR>
    <TD align=left width=90><SPAN id=submenu4 
      style="DISPLAY: none; MARGIN-LEFT: 5px"><BR><A 
      href="Encyclopedia_RuleList.asp?RuleType=4" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;阵法计略</A><BR><A 
      href="Encyclopedia_RuleList.asp?RuleType=3" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;战斗计略</A><BR><A 
      href="Encyclopedia_RuleList.asp?RuleType=5" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;录用计略</A><BR><A 
      href="Encyclopedia_RuleList.asp?RuleType=1" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;本国计略</A><BR><A 
      href="Encyclopedia_RuleList.asp?RuleType=2" 
      target=Encyclopediaright><FONT 
      color=#ebc550></FONT>&nbsp;&nbsp;他国计略</A><BR></SPAN></TD></TR>
  <TR>
    <TD><A 
      href="Encyclopedia_SirTitleList.asp" 
      target=Encyclopediaright><SPAN language=javascript id=bar5 
      style="CURSOR: hand" onClick="menuclick(submenu5, bar5);"><IMG 
      src="images/encyb5.gif" border=0 
name=nc4></SPAN></A></TD></TR>
  <TR>
    <TD align=left width=90><SPAN id=submenu5 
      style="DISPLAY: none; MARGIN-LEFT: 5px"></SPAN></TD></TR>
	<TR>
    <TD><A 
      href="Encyclopedia_PowerList.asp" 
      target=Encyclopediaright><SPAN language=javascript id=bar6 
      style="CURSOR: hand" onClick="menuclick(submenu6, bar6);"><IMG 
      src="images/encyb6.gif" border=0 
name=nc4></SPAN></A></TD></TR> 
<TR>
    <TD align=left width=90><SPAN id=submenu5 
      style="DISPLAY: none; MARGIN-LEFT: 5px"></SPAN></TD></TR>
	  </TBODY></TABLE><FONT 
color=black>13</FONT> </BODY></HTML>


