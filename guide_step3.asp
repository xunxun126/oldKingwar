<!--#include file="inc/Biz_Inc.asp" -->
<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="14" valign="top" background="Images/2007/OWARSbb.jpg"><img name="OWARSbb" src="Images/2007/OWARSbb.jpg" width="14" height="211" border="0" id="OWARSbb" alt="" /></td>
    <td width="732" valign="top" bgcolor="#F4EBDA"><table width="732" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" valign="top"><DIV align=center>
          <table width="400" height="42" border="0" cellpadding="10" cellspacing="1" bgcolor="#CABF8E">
            <tr>
              <td align="center" bgcolor="#EBE7D3"><strong>三国义志 新手上路3 </strong></td>
            </tr>
          </table>
          <table width="50%"  border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <TABLE cellSpacing=0 cellPadding=0 width=650 align=center border=0>
            <TBODY>
              <TR>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step1.asp"><IMG 
            src="images/guide_step_b1.gif" border=0><BR>
                      <FONT 
            color=#a6a6a6><B>登陆方法</B></FONT></A></TD>
                <TD align=middle width=15><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step2.asp"><IMG 
            src="images/guide_step_b2.gif" border=0><BR>
                      <FONT 
            color=#a6a6a6><B>选择君主</B></FONT></A></TD>
                <TD align=middle width=14><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step3.asp"><IMG 
            height=29 src="images/guide_step3.gif" width=55 
            border=0><BR>
                      <FONT color=#5d5d5d><B>国家初期建设</B></FONT></A></TD>
                <TD align=middle width=14><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step4.asp"><IMG 
            height=29 src="images/guide_step_b4.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>录用将帅</B></FONT></A></TD>
                <TD align=middle width=14><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step5.asp"><IMG 
            height=29 src="images/guide_step_b5.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>战争准备</B></FONT></A></TD>
                <TD align=middle width=14><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step6.asp"><IMG 
            height=29 src="images/guide_step_b6.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>战争</B></FONT></A></TD>
                <TD align=middle width=15><IMG 
            src="images/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step7.asp"><IMG 
            height=29 src="images/guide_step_b7.gif" width=55 
            border=0><BR>
                      <FONT 
      color=#a6a6a6><B>其他</B></FONT></A></TD>
              </TR>
            </TBODY>
          </TABLE>
          <table width="400" height="18" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <TABLE cellSpacing=0 cellPadding=0 width="90%" border=0>
            <TBODY>
              <TR>
                <TD colSpan=2 height=25><FONT color=#fb3f00><B>3. 国家初期运营</B></FONT> <FONT 
            color=#006699>&nbsp;&nbsp;&nbsp;&nbsp;开拓&gt;建设&gt;研究政策&gt;实行政策</FONT> </TD>
              </TR>
              <TR>
                <TD colSpan=2 height=25><FONT color=#fb3f00>&nbsp;&nbsp; (1)开拓和建设</FONT></TD>
              </TR>
              <TR>
                <TD width="5%"></TD>
                <TD><FONT color=#000000>关于初期保护： 初期保护状态下您的国家是不会受到其它国家攻击的,当然也无法攻击其它国家.<BR>
                  初期保护解除状态分4种：(1) 使用能量超过1000. &nbsp;&nbsp;(2) 兵力超过10000名.&nbsp;&nbsp;(3)国土超过5000&nbsp;&nbsp; (4)战争 <BR>
                  关于能量： 能量四分钟自动产生一个能量，上限360能量，会员二分钟自动产生一个能量，会员上限720时能量。。能量是自动产生的，不管在不在线。<BR>
                  <BR>
                  游戏刚开始时会给予玩家500的国土, 300的能量, 1,000,000的粮食。<BR>
                  1 民忠：民忠是交纳税金的关键。并影响到君主的名声。要经常维持到95以上。可以通过《实施政策》中《节日》来增加或者从《政策》里的〈救济〉这项来增加民忠值。<BR>
                  <BR>
                  民忠上涨------国家的财政收入上升，粮食与资金收入也会增多。<BR>
                  每使用一点的能量值君主的名声都会+3。<BR>
                  民忠减少------财政会出现危机,粮食，资金减少，国家走向灭亡。<BR>
                  <BR>
                  2开拓：100~200个能量可以开拓出2000~3000的荒地，在这个基础上在去建造其它的基本设施。<BR>
                  <BR>
                  所要建造的基本设施如下：<BR>
                  <BR>
                  <BR>
                  <BR>
                  <FONT 
            color=#0069b3><B>工厂</B></FONT><BR>
                  开始要先建造工厂，因为随着工厂数量的增多,其它建筑物的标准数量也会增加.<BR>
                  <FONT 
            color=#006699>※ 工厂的数量为0的情况下,建造其他建筑物就会很费时间.</FONT><BR>
                  .建造工厂要一个能量一个能量的使用，注意对应每一个能量可以建造的工厂数量。绝对不要一次性全部建完，注意能量的合理运用。建议数量：600个（参考）。<BR>
                  <BR>
                  <FONT 
            color=#0069b3><B>田地</B></FONT><BR>
                  生产军粮的基地，粮食生产越多越能培养强大的队伍。一块田地需要500人耕种。建议数量：650（参考）。<BR>
                  <BR>
                  <FONT 
            color=#0069b3><B>村庄</B></FONT><BR>
                  百姓居住的地方. 创造生存的空间就必须建造村庄,村庄数量越多百姓的数量就会增加,一个村庄可以容纳 500人。建议数量：600个（参考）。<BR>
                  <BR>
                  <FONT 
            color=#0069b3><B>训练所</B></FONT><BR>
                  征集军队的地方,训练所越多能培养军队的数量也就越多。建议数量：500个（参考）。<BR>
                  <BR>
                  <FONT 
            color=#0069b3><B>市场</B></FONT><BR>
                  市场与国土相比不到10%不能去市场。君主本人无法去市场，必须使用雇佣的将帅去。资金、随机项目的出现、建筑物的维持、将帅维持费、购买贵重的物品、购买紧急军粮,等都与市场有关.一个市场需要500人进行工作。建议数量：600个（参考）。<BR>
                  <BR>
                  <FONT 
            color=#0069b3><B>城</B></FONT><BR>
                  城与国家的存亡有关。一个城只可以雇佣一名将帅。<BR>
                  <FONT 
            color=#006699>※ 城的数量为0时，国家将走向灭亡.</FONT><BR>
                  <BR>
                </FONT></TD>
              </TR>
              <TR>
                <TD colSpan=2 height=25><FONT color=#fb3f00>&nbsp;&nbsp; (2)实行政策 </FONT></TD>
              </TR>
              <TR>
                <TD width="5%"></TD>
                <TD><FONT 
            color=#000000>实行政策的时候,每用1点的能量国家的资源就会有相应的变化.实行政策后会从相应的数据中看到政策实行的结果（例如：国家资金的上涨、民忠的上涨、军队的增多等。） <BR>
                      <BR>
                      <FONT 
            color=#0069b3><B>阵法</B></FONT><BR>
                  在战争中，被使用的阵法。<BR>
                  <BR>
                  <FONT 
            color=#0069b3><B>兵法</B></FONT><BR>
                  在战争中，被使用的兵法。<BR>
                  <BR>
                </FONT></TD>
              </TR>
              <TR>
                <TD colSpan=2 height=25><FONT color=#fb3f00>&nbsp;&nbsp; (3)内政 </FONT></TD>
              </TR>
              <TR>
                <TD width="5%"></TD>
                <TD><FONT 
            color=#000000>利用现有的土地,进行田地、村庄、市场、训练所、等设施的建设。<BR>
                  您选择的君主不同所实行的基本政策也不尽相同。<BR>
                  共通的政策：节日、推荐、固定市场，义勇军云集、急求军粮 确保军费。<BR>
                  特殊的政策：（ 以曹操为例）九品官人法。<BR>
                  其他政策： 暴政 、物产奖励。<BR>
                  <BR>
                </FONT></TD>
              </TR>
            </TBODY>
          </TABLE>
        </DIV></td>
      </tr>
    </table></td>
    <td width="14" valign="top" background="Images/2007/OWARSbf.jpg"><img name="OWARSbf" src="Images/2007/OWARSbf.jpg" width="14" height="388" border="0" id="OWARSbf" alt="" /></td>
  </tr>
</table>
<!--#include file="inc/Biz_IncFooter.asp" -->

