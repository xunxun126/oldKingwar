<!--#include file="inc/Biz_Inc.asp" -->
<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="14" valign="top" background="Images/2007/OWARSbb.jpg"><img name="OWARSbb" src="Images/2007/OWARSbb.jpg" width="14" height="211" border="0" id="OWARSbb" alt="" /></td>
    <td width="732" valign="top" bgcolor="#F4EBDA"><table width="732" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" valign="top"><DIV align=center>
          <table width="400" height="42" border="0" cellpadding="10" cellspacing="1" bgcolor="#CABF8E">
            <tr>
              <td align="center" bgcolor="#EBE7D3"><strong>三国义志 新手上路5 </strong></td>
            </tr>
          </table>
          <table width="50%"  border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <TABLE cellSpacing=0 cellPadding=0 width=650 align=center border=0>
            <TBODY>
              <TR>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step1.asp"><IMG 
            src="IMAGES/guide_step_b1.gif" border=0><BR>
                      <FONT 
            color=#a6a6a6><B>登陆方法</B></FONT></A></TD>
                <TD align=middle width=15><IMG src="IMAGES/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step2.asp"><IMG 
            src="IMAGES/guide_step_b2.gif" border=0><BR>
                      <FONT 
            color=#a6a6a6><B>选择君主</B></FONT></A></TD>
                <TD align=middle width=14><IMG src="IMAGES/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step3.asp"><IMG 
            height=29 src="IMAGES/guide_step_b3.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>国家初期建设</B></FONT></A></TD>
                <TD align=middle width=14><IMG src="IMAGES/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step4.asp"><IMG 
            height=29 src="IMAGES/guide_step_b4.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>录用将帅</B></FONT></A></TD>
                <TD align=middle width=14><IMG src="IMAGES/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step5.asp"><IMG 
            height=29 src="IMAGES/guide_step5.gif" width=55 border=0><BR>
                      <FONT 
            color=#5d5d5d><B>战争准备</B></FONT></A></TD>
                <TD align=middle width=14><IMG src="IMAGES/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step6.asp"><IMG 
            height=29 src="IMAGES/guide_step_b6.gif" width=55 
            border=0><BR>
                      <FONT color=#a6a6a6><B>战争</B></FONT></A></TD>
                <TD align=middle width=15><IMG src="IMAGES/arrow.gif"><BR>
                    <BR></TD>
                <TD vAlign=top align=middle width=80><A onfocus=this.blur() 
            href="guide_step7.asp"><IMG 
            height=29 src="IMAGES/guide_step_b7.gif" width=55 
            border=0><BR>
                      <FONT 
      color=#a6a6a6><B>其他</B></FONT></A></TD>
              </TR>
            </TBODY>
          </TABLE>
          <table width="400" height="18" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
          <TABLE cellSpacing=0 cellPadding=0 width="90%" border=0>
            <TBODY>
              <TR>
                <TD colSpan=2 height=25><FONT color=#fb3f00><B>5. 战争准备</B></FONT> <FONT 
            color=#006699>&nbsp;&nbsp;&nbsp;&nbsp;补充气力&gt;士气振作&gt;征集&gt;兵力分配&gt;形势图&gt;战争</FONT></TD>
              </TR>
              <TR>
                <TD 
            colSpan=2>出征之前要补充精力.精力必须超过20才可出征.战争时精力最好维持在70左右.通过《士气振作》将士气补充为200.在三网中士气高低非常重要.士气如果是两倍的话,攻击力也会变为两倍.所以最好将士气经常维持在200左右。<BR>
                    <BR></TD>
              </TR>
              <TR>
                <TD colSpan=2 height=25><FONT color=#fb3f00>&nbsp;&nbsp; (1) 征集</FONT></TD>
              </TR>
              <TR>
                <TD width="5%"></TD>
                <TD>官位不同所征兵种也不同<BR>
                    <BR>
                    <FONT color=#0069b3><B>太守 : </B></FONT>能雇佣轻骑兵,弓兵,短兵.<BR>
                    <FONT color=#0069b3><B>牧 &nbsp;&nbsp;: </B></FONT>能雇佣重骑兵,连弩兵,冲车与望楼队.<BR>
                    <FONT color=#0069b3><B>王 &nbsp;&nbsp;: </B></FONT>能雇佣发石车,连弩队,战车,武林高手,枪兵.<BR>
                    <FONT 
            color=#0069b3><B>丞相 : </B></FONT>能雇佣亲卫骑兵,亲卫步兵,亲卫弓兵.<BR>
                    <FONT 
            color=#0069b3><B>皇帝 : </B></FONT>能雇佣御林骑兵,御林步兵,御林弓兵.<BR>
                    <BR></TD>
              </TR>
              <TR>
                <TD colSpan=2 height=25><FONT color=#fb3f00>&nbsp;&nbsp; (2) 兵力配置 </FONT></TD>
              </TR>
              <TR>
                <TD width="5%"></TD>
                <TD><p>分配给各将帅。注意进攻与防御军队的分配。<BR>
                  <BR>
                  <FONT color=#0069b3><B>军队的种类 : </B></FONT>将帅要率领的军队.<BR>
                  <FONT color=#0069b3><B>指挥率 : </B></FONT>将帅带领军队的比率.设定可以从0～100%.<BR>
                </p>
                  <p><img src="Images/Help/pz.gif" width="603" height="375"></p>
                  <p>&nbsp;</p></TD>
              </TR>
              <TR>
                <TD height="14" colspan="2"><strong><font color="#FF9900">3)将帅的任务</font></strong></TD>
                </TR>
              <TR>
                <TD></TD>
                <TD><p><span class="style12">军师:</span>只能设一个军师,一般用智力最高者,这样可在战争时抵抗和使用计策.</p>
                  <p><span class="style12">巡查国境:</span>当设为巡查国境时武将才会出战.</p>
                  <p><span class="style12">同盟协防:</span>当国家于别的国家同盟时,只有设为同盟协防的武将才会参于盟国战争,另外这个武将只协防同盟作战,不参加本国战争.</p></TD>
              </TR>
            </TBODY>
          </TABLE>
        </DIV></td>
      </tr>
    </table></td>
    <td width="14" valign="top" background="Images/2007/OWARSbf.jpg"><img name="OWARSbf" src="Images/2007/OWARSbf.jpg" width="14" height="388" border="0" id="OWARSbf" alt="" /></td>
  </tr>
</table>
<!--#include file="inc/Biz_IncFooter.asp" -->

